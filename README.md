# Spletni barvni stroboskop

1\. domača naloga pri predmetu [Osnove informacijskih sistemov](https://ucilnica.fri.uni-lj.si/course/view.php?id=54) (navodila)


## 6.1 Opis naloge in navodila

Na Bitbucket je na voljo javni repozitorij [https://bitbucket.org/asistent/stroboskop](https://bitbucket.org/asistent/stroboskop), ki vsebuje nedelujoč spletni stroboskop. V okviru domače naloge ustvarite kopijo repozitorija ter popravite in dopolnite obstoječo implementacijo spletne strani tako, da bo končna aplikacija z vsemi delujočimi funkcionalnostimi izgledala kot na spodnji sliki. Natančna navodila si preberite na [spletni učilnici](https://ucilnica.fri.uni-lj.si/mod/workshop/view.php?id=19181).

![Stroboskop](stroboskop.gif)

## 6.2 Vzpostavitev repozitorija

Naloga 6.2.2:

```
git clone https://CyberRaven@bitbucket.org/CyberRaven/stroboskop.git
```

Naloga 6.2.3:

https://bitbucket.org/CyberRaven/stroboskop/commits/58ca5e1fe81a7e533e8907ea9d66752829675282

## 6.3 Skladnja strani in stilska preobrazba

Naloga 6.3.1:
https://bitbucket.org/CyberRaven/stroboskop/commits/6f1ce4aba77f48eaa47895a34c8d5f74f51b7ea7

Naloga 6.3.2:
https://bitbucket.org/CyberRaven/stroboskop/commits/7cdb6c3f554cf7a8176e60ce419e098ad3380d56

Naloga 6.3.3:
https://bitbucket.org/CyberRaven/stroboskop/commits/0c75e09d38a749f022d9cab1add6b793587a55a3

Naloga 6.3.4:
https://bitbucket.org/CyberRaven/stroboskop/commits/5b65a03919f646b38ab2bdc5643b6cbb86643bf6

Naloga 6.3.5:

```
git checkout master
git merge izgled
git push
```

## 6.4 Dinamika in animacija na strani

Naloga 6.4.1:
https://bitbucket.org/CyberRaven/stroboskop/commits/9fc648b18900fc5a18f4ed6e9586fdd6f0391fcd

Naloga 6.4.2:
https://bitbucket.org/CyberRaven/stroboskop/commits/3f6e0380106831b1614b33b295a8804b9e734754

Naloga 6.4.3:
https://bitbucket.org/CyberRaven/stroboskop/commits/56967e32729652c2f2c6235579738ccbe0db9f70

Naloga 6.4.4:
https://bitbucket.org/CyberRaven/stroboskop/commits/f3a1ec33b3a45a0758b272ee24fa9c86a6b70a5a